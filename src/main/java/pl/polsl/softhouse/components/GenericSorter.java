package pl.polsl.softhouse.components;

import org.springframework.stereotype.Component;
import pl.polsl.softhouse.entities.AbstractWorkUnit;
import pl.polsl.softhouse.entities.enums.SortingOption;

import java.util.Comparator;

@Component
public class GenericSorter <T extends AbstractWorkUnit> {
    public Comparator<T> getComparator(SortingOption sortBy) {
        if(sortBy != null) {
            switch (sortBy) {
                case NAME_ASC:
                    return Comparator.comparing(u -> u.getName().toLowerCase());
                case NAME_DESC:
                    return (u1, u2) -> u2.getName().toLowerCase().compareTo(u1.getName().toLowerCase());
                case DATE_ASC:
                    return Comparator.comparing(AbstractWorkUnit::getDateOpened);
                case DATE_DESC:
                    return (u1, u2) -> u2.getDateOpened().compareTo(u1.getDateOpened());
            }
        }
        return null;
    }
}
