package pl.polsl.softhouse.components.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import pl.polsl.softhouse.entities.UserEntity;
import pl.polsl.softhouse.entities.enums.UserRole;
import pl.polsl.softhouse.exceptions.user.UserNotFoundException;
import pl.polsl.softhouse.repositories.UserRepository;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class UserEntityDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    public UserEntityDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userRepository.findByEmail(username)
                .orElseThrow(() -> UserNotFoundException.fromUsername(username));
        boolean active = user.getActive();
        GrantedAuthority userRole = roles.getOrDefault(user.getRole(), new SimpleGrantedAuthority(SecurityRole.ANONYMOUS));

        return new User(user.getEmail(),
                user.getPassword(),
                active,
                active,
                active,
                active,
                List.of(userRole)
        );
    }
    private static final Map<UserRole, GrantedAuthority> roles = Arrays.stream(UserRole.values())
            .collect(Collectors.toMap(
                    role -> role,
                    role -> new SimpleGrantedAuthority("ROLE_" + role.toString())
            ));
}
