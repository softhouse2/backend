package pl.polsl.softhouse.components.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import pl.polsl.softhouse.dto.issue.IssueMapper;
import pl.polsl.softhouse.dto.request.RequestMapper;
import pl.polsl.softhouse.dto.task.TaskMapper;
import pl.polsl.softhouse.entities.Issue;
import pl.polsl.softhouse.entities.Request;
import pl.polsl.softhouse.entities.Task;
import pl.polsl.softhouse.entities.UserEntity;
import pl.polsl.softhouse.exceptions.AuthenticationException;
import pl.polsl.softhouse.exceptions.AuthorizationException;
import pl.polsl.softhouse.repositories.UserRepository;

@Component
public class PermissionGuard {

    private final UserRepository userRepository;

    public PermissionGuard(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void checkRequestUpdatePermOrThrow(Request request) {
        UserEntity currentUser = fetchCurrentUser();

        switch (currentUser.getRole()) {
            case ADMIN:
                return;
            case ACCOUNT_MANAGER:
                if (RequestMapper.mapAllowedUsers(request).contains(currentUser.getId())) {
                    return;
                }
                break;
            case PRODUCT_MANAGER:
            case WORKER:
                break;
        }

        throw new AuthorizationException("Invalid permissions for Request update");
    }

    public void checkIssueUpdatePermOrThrow(Issue issue) {
        UserEntity currentUser = fetchCurrentUser();

        switch (currentUser.getRole()) {
            case ADMIN:
                return;
            case ACCOUNT_MANAGER:
            case PRODUCT_MANAGER:
                if (IssueMapper.mapAllowedUsers(issue).contains(currentUser.getId())) {
                    return;
                }
                break;
            case WORKER:
                break;
        }

        throw new AuthorizationException("Invalid permissions for Issue update");
    }

    public void checkTaskChangePermOrThrow(Task task) {
        UserEntity currentUser = fetchCurrentUser();

        switch (currentUser.getRole()) {
            case ADMIN:
                return;
            case ACCOUNT_MANAGER:
            case PRODUCT_MANAGER:
            case WORKER:
                if (TaskMapper.mapAllowedUsers(task).contains(currentUser.getId())) {
                    return;
                }
                break;
        }

        throw new AuthorizationException("Invalid permissions for Task update");
    }

    public void checkPasswordUpdatePermOrThrow() {
        UserEntity currentUser = fetchCurrentUser();

        switch (currentUser.getRole()) {
            case ADMIN:
                return;
            case ACCOUNT_MANAGER:
            case PRODUCT_MANAGER:
            case WORKER:
                break;
        }

        throw new AuthorizationException("Invalid permissions for User update");
    }

    private UserEntity fetchCurrentUser() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();

        return userRepository.findByEmail(email)
                .orElseThrow(() -> new AuthenticationException("No current user"));
    }
}
