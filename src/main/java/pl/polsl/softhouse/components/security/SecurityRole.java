package pl.polsl.softhouse.components.security;

public interface SecurityRole {
    String ADMIN = "ROLE_ADMIN";
    String ACCOUNT_MANAGER = "ROLE_ACCOUNT_MANAGER";
    String PRODUCT_MANAGER = "ROLE_PRODUCT_MANAGER";
    String WORKER = "ROLE_WORKER";
    String ANONYMOUS = "ROLE_ANONYMOUS";
}
