package pl.polsl.softhouse.components.statusListeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.polsl.softhouse.entities.Issue;
import pl.polsl.softhouse.entities.Request;
import pl.polsl.softhouse.entities.Task;
import pl.polsl.softhouse.exceptions.issue.IssueNotFoundException;
import pl.polsl.softhouse.exceptions.request.RequestNotFoundException;
import pl.polsl.softhouse.repositories.IssueRepository;
import pl.polsl.softhouse.repositories.RequestRepository;
import pl.polsl.softhouse.repositories.TaskRepository;

@Component
public class StatusListener {
    private final TaskRepository taskRepository;
    private final IssueRepository issueRepository;
    private final RequestRepository requestRepository;

    @Autowired
    public StatusListener(TaskRepository taskRepository, IssueRepository issueRepository, RequestRepository requestRepository) {
        this.taskRepository = taskRepository;
        this.issueRepository = issueRepository;
        this.requestRepository = requestRepository;
    }

    public void checkAndPropagateCancelling(Request request) {
        for(Issue issue : request.getIssues()) {
            issue.setStatus(request.getStatus());
            issueRepository.save(issue);
            checkAndPropagateCancelling(issue);
        }
    }

    public void checkAndPropagateCancelling(Issue issue) {
        for(Task task : issue.getTasks()) {
            task.setStatus(issue.getStatus());
            taskRepository.save(task);
        }
    }

    public void checkAndPropagateInProgress(Task task) {
        Issue issue = issueRepository.findById(task.getIssue().getId()).orElseThrow(()
                -> new IssueNotFoundException(task.getIssue().getId()));
        issue.setStatus(task.getStatus());
        issueRepository.save(issue);
        checkAndPropagateInProgress(issue);
    }

    public void checkAndPropagateInProgress(Issue issue) {
        Request request = requestRepository.findById(issue.getRequest().getId()).orElseThrow(()
                -> new RequestNotFoundException(issue.getRequest().getId()));
        request.setStatus(issue.getStatus());
        requestRepository.save(request);
    }
}
