package pl.polsl.softhouse.components.seeders;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import pl.polsl.softhouse.dto.task.TaskPostDto;
import pl.polsl.softhouse.entities.enums.TaskType;
import pl.polsl.softhouse.entities.enums.WorkPriority;
import pl.polsl.softhouse.entities.enums.WorkStatus;
import pl.polsl.softhouse.services.TaskService;

import java.util.List;

@Component
@Order(4)
public class TaskDataSeeder extends WorkSeederBase implements CommandLineRunner {

    private final TaskService taskService;

    public TaskDataSeeder(Environment env, TaskService taskService) {
        super(env);
        this.taskService = taskService;
    }

    @Override
    public void run(String... args) {
        if (shouldSeedObjects()) {
            seedTasks();
        }
    }

    private void seedTasks() {
        List<TaskPostDto> requests = List.of(
                new TaskPostDto(
                        WorkStatus.IN_PROGRESS,
                        "",
                        NOW.minusDays(LOW_PRIORITY_DAYS - 5),
                        NOW,
                        null,
                        NOW.plusDays(5),
                        WorkPriority.NORMAL,
                        "TestTask1",
                        TaskType.ANALYSIS,
                        4L,
                        1L),
                new TaskPostDto(
                        WorkStatus.OPEN,
                        "",
                        NOW,
                        null,
                        null,
                        NOW.plusDays(NORMAL_PRIORITY_DAYS),
                        WorkPriority.NORMAL,
                        "TestTask2",
                        TaskType.DESIGN,
                        4L,
                        2L)
        );

        requests.forEach(taskService::addTask);
    }
}