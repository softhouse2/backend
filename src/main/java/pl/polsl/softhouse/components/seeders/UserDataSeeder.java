package pl.polsl.softhouse.components.seeders;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import pl.polsl.softhouse.components.seeders.config.UserSeederConfig;
import pl.polsl.softhouse.entities.enums.UserRole;
import pl.polsl.softhouse.services.UserService;

@Component
@Order(0)
public class UserDataSeeder implements CommandLineRunner {

    private final UserService userService;

    private final UserSeederConfig config;

    public UserDataSeeder(UserService userService, UserSeederConfig config) {
        this.userService = userService;
        this.config = config;
    }

    @Override
    public void run(String... args) {
        if (config.shouldSeedUsers()) {
            seedUsers();
        }
    }

    private void seedUsers() {
        for (UserRole role : UserRole.values()) {
            userService.addUser(config.getUser(role));
        }
    }
}
