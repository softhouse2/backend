package pl.polsl.softhouse.components.seeders.config;

import org.springframework.core.env.Environment;
public abstract class PropertyConfigBase {

    protected final Environment env;

    protected static final String INIT_ROOT = "init";
    protected static final String SEED_PROP = "seed";

    public PropertyConfigBase(Environment env) {
        this.env = env;
    }

    public static String resolvePropertyPath(String ...segments) {
        if (segments.length < 1) {
            return null;
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < segments.length - 1; i++) {
            sb.append(segments[i]).append(".");
        }
        sb.append(segments[segments.length - 1]);

        return sb.toString();
    }

    protected boolean checkTrue(String property) {
        String propValue = env.getProperty(property);
        if (propValue == null) {
            return false;
        }
        return Boolean.parseBoolean(propValue);
    }
}
