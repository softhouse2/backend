package pl.polsl.softhouse.components.seeders;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import pl.polsl.softhouse.dto.client.ClientPostDto;
import pl.polsl.softhouse.dto.system.SystemPostDto;
import pl.polsl.softhouse.dto.systemVersion.SystemVersionPostDto;
import pl.polsl.softhouse.services.ClientService;
import pl.polsl.softhouse.services.SystemVersionService;
import pl.polsl.softhouse.services.SystemService;

import java.util.ArrayList;
import java.util.List;

@Component
@Order(1)
public class SystemVersionDataSeeder extends DebugSeederBase implements CommandLineRunner {

    private final SystemService systemService;
    private final ClientService clientService;
    private final SystemVersionService systemVersionService;

    private final List<Long> systemIds = new ArrayList<>();
    private final List<Long> clientIds = new ArrayList<>();

    public SystemVersionDataSeeder(SystemService systemService,
                                   ClientService clientService,
                                   SystemVersionService systemVersionService,
                                   Environment env) {

        super(env);
        this.systemService = systemService;
        this.clientService = clientService;
        this.systemVersionService = systemVersionService;
    }

    @Override
    public void run(String... args) {
        if (shouldSeedObjects()) {
            seedSystems();
            seedClients();
            seedSystemVersions();
        }
    }

    private void seedSystems() {
        List<SystemPostDto> systems = List.of(
                new SystemPostDto("First Seeded System"),
                new SystemPostDto("Seeded System #2"),
                new SystemPostDto("SysSeed3"),
                new SystemPostDto("4th SeedSystem")
        );

        systems.forEach(system -> systemIds.add(
                systemService.addSystem(system)
        ));
    }

    private void seedClients() {
        List<ClientPostDto> clients = List.of(
                new ClientPostDto("Client McClient"),
                new ClientPostDto("Maria VonClientess")
        );

        clients.forEach(client -> clientIds.add(
                clientService.addClient(client)
        ));
    }

    private void seedSystemVersions() {
        List<SystemVersionPostDto> systemVersions = List.of(
                new SystemVersionPostDto(systemIds.get(0), clientIds.get(0), "1.0.0"),
                new SystemVersionPostDto(systemIds.get(0), clientIds.get(1), "1.2.0"),
                new SystemVersionPostDto(systemIds.get(1), clientIds.get(1), "0.5.4"),
                new SystemVersionPostDto(systemIds.get(2), clientIds.get(0), "2.1"),
                new SystemVersionPostDto(systemIds.get(2), clientIds.get(1), "2.4"),
                new SystemVersionPostDto(systemIds.get(3), clientIds.get(1), "0.3B")
        );

        systemVersions.forEach(systemVersionService::addSystemVersion);
    }
}
