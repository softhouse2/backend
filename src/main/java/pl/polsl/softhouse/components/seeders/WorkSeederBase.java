package pl.polsl.softhouse.components.seeders;

import org.springframework.core.env.Environment;

import java.time.LocalDateTime;

public abstract class WorkSeederBase extends DebugSeederBase {

    protected static final int LOW_PRIORITY_DAYS = 14;
    protected static final int NORMAL_PRIORITY_DAYS = 7;
    protected static final int HIGH_PRIORITY_DAYS = 3;
    protected static final LocalDateTime NOW = LocalDateTime.now();

    public WorkSeederBase(Environment env) {
        super(env);
    }
}
