package pl.polsl.softhouse.components.seeders;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import pl.polsl.softhouse.dto.request.RequestPostDto;
import pl.polsl.softhouse.entities.enums.WorkPriority;
import pl.polsl.softhouse.entities.enums.WorkStatus;
import pl.polsl.softhouse.services.RequestService;

import java.util.List;

@Component
@Order(2)
public class RequestDataSeeder extends WorkSeederBase implements CommandLineRunner {

    private final RequestService requestService;

    public RequestDataSeeder(Environment env, RequestService requestService) {
        super(env);
        this.requestService = requestService;
    }

    @Override
    public void run(String... args) {
        if (shouldSeedObjects()) {
            seedRequests();
        }
    }

    private void seedRequests() {
        List<RequestPostDto> requests = List.of(
                new RequestPostDto(
                        2L,
                        "This is the first test request",
                        1L,
                        WorkStatus.OPEN,
                        "TestRequest1",
                        NOW,
                        null,
                        null,
                        NOW.plusDays(NORMAL_PRIORITY_DAYS),
                        WorkPriority.NORMAL,
                        ""),
                new RequestPostDto(
                        2L,
                        "This is the second test request",
                        2L,
                        WorkStatus.FINISHED,
                        "TestRequest2",
                        NOW.minusDays(NORMAL_PRIORITY_DAYS),
                        NOW.minusDays(2),
                        NOW.minusDays(1),
                        NOW,
                        WorkPriority.NORMAL,
                        "This request has been finished successfully")
                );

        requests.forEach(requestService::addRequest);
    }
}