package pl.polsl.softhouse.components.seeders;

import org.springframework.core.env.Environment;
import pl.polsl.softhouse.components.seeders.config.PropertyConfigBase;

public abstract class DebugSeederBase extends PropertyConfigBase {

    protected final String DATA_PROP = "data";

    public DebugSeederBase(Environment env) {
        super(env);
    }

    protected boolean shouldSeedObjects() {
        String prop = resolvePropertyPath(INIT_ROOT, SEED_PROP, DATA_PROP);
        return checkTrue(prop);
    }
}
