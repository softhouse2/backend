package pl.polsl.softhouse.components.seeders.config;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import pl.polsl.softhouse.dto.user.UserPostDto;
import pl.polsl.softhouse.entities.enums.UserRole;

import java.util.AbstractMap;
import java.util.Map;

@Component
public class UserSeederConfig extends PropertyConfigBase {

    private final String USERS_PROP = "users";
    private final String PASSWORD_PROP = "password";

    private final Map<UserRole, String> rolePrefixes = Map.ofEntries(
            new AbstractMap.SimpleImmutableEntry<>(UserRole.ADMIN, "admin"),
            new AbstractMap.SimpleImmutableEntry<>(UserRole.ACCOUNT_MANAGER, "acc_man"),
            new AbstractMap.SimpleImmutableEntry<>(UserRole.PRODUCT_MANAGER, "prod_man"),
            new AbstractMap.SimpleImmutableEntry<>(UserRole.WORKER, "worker")
    );

    private final Map<UserRole, String> defaultEmails = Map.ofEntries(
            new AbstractMap.SimpleImmutableEntry<>(UserRole.ADMIN, "admin@softhouse.com"),
            new AbstractMap.SimpleImmutableEntry<>(UserRole.ACCOUNT_MANAGER, "accmanager@softhouse.com"),
            new AbstractMap.SimpleImmutableEntry<>(UserRole.PRODUCT_MANAGER, "prodmanager@softhouse.com"),
            new AbstractMap.SimpleImmutableEntry<>(UserRole.WORKER, "worker@softhouse.com")
    );

    private final String globalPassword;

    public UserSeederConfig(Environment env) {
        super(env);

        String globalPassProp = resolvePropertyPath(INIT_ROOT, USERS_PROP, PASSWORD_PROP);
        if (globalPassProp != null) {
            globalPassword = env.getProperty(globalPassProp);
        } else {
            globalPassword = null;
        }
    }

    public UserPostDto getUser(UserRole role) {

        String password = getPassword(role);

        switch (role) {
            case ADMIN:
                return getAdmin(password);
            case ACCOUNT_MANAGER:
                return getAccountManager(password);
            case PRODUCT_MANAGER:
                return getProductManager(password);
            case WORKER:
                return getWorker(password);
        }

        return null;
    }

    private UserPostDto getAdmin(String password) {
        return new UserPostDto(
                getEmail(UserRole.ADMIN),
                password,
                true,
                "Admin",
                "Admin",
                UserRole.ADMIN,
                "000000000");
    }

    private UserPostDto getAccountManager(String password) {
        return new UserPostDto(
                getEmail(UserRole.ACCOUNT_MANAGER),
                password,
                true,
                "Account",
                "Manager",
                UserRole.ACCOUNT_MANAGER,
                "000000000");
    }

    private UserPostDto getProductManager(String password) {
        return new UserPostDto(
                getEmail(UserRole.PRODUCT_MANAGER),
                password,
                true,
                "Product",
                "Manager",
                UserRole.PRODUCT_MANAGER,
                "000000000");
    }

    private UserPostDto getWorker(String password) {
        return new UserPostDto(
                getEmail(UserRole.WORKER),
                password,
                true,
                "Worker",
                "Worker",
                UserRole.WORKER,
                "000000000");
    }

    private String getPassword(UserRole role) {

        String password = null;
        String rolePrefix = rolePrefixes.get(role);

        String passwordProp = resolvePropertyPath(INIT_ROOT, USERS_PROP, rolePrefix, PASSWORD_PROP);
        if (passwordProp != null) {
            password = env.getProperty(passwordProp);
        }

        if (password != null) {
            return password;
        }
        if (globalPassword != null) {
            return globalPassword;
        }
        return "password";
    }

    private String getEmail(UserRole role) {
        String email = null;
        String rolePrefix = rolePrefixes.get(role);

        String emailProp = resolvePropertyPath(INIT_ROOT, USERS_PROP, rolePrefix, "email");
        if (emailProp != null) {
            email = env.getProperty(emailProp);
        }

        if (email != null) {
            return email;
        }
        return defaultEmails.get(role);
    }

    public boolean shouldSeedUsers() {
        String prop = resolvePropertyPath(INIT_ROOT, SEED_PROP, USERS_PROP);
        return checkTrue(prop);
    }
}
