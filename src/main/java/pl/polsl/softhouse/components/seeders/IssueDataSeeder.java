package pl.polsl.softhouse.components.seeders;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import pl.polsl.softhouse.dto.issue.IssuePostDto;
import pl.polsl.softhouse.entities.enums.WorkPriority;
import pl.polsl.softhouse.entities.enums.WorkStatus;
import pl.polsl.softhouse.entities.enums.IssueType;
import pl.polsl.softhouse.services.IssueService;

import java.util.List;

@Component
@Order(3)
public class IssueDataSeeder extends WorkSeederBase implements CommandLineRunner {

    private final IssueService issueService;

    public IssueDataSeeder(Environment env, IssueService issueService) {
        super(env);
        this.issueService = issueService;
    }

    @Override
    public void run(String... args) {
        if (shouldSeedObjects()) {
            seedIssues();
        }
    }

    private void seedIssues() {
        List<IssuePostDto> requests = List.of(
                new IssuePostDto(
                        WorkStatus.OPEN,
                        "TestIssue1",
                        NOW,
                        null,
                        null,
                        NOW.plusDays(NORMAL_PRIORITY_DAYS),
                        WorkPriority.NORMAL,
                        "",
                        "This is the first test issue",
                        IssueType.FEATURE,
                        1L,
                        3L),
                new IssuePostDto(
                        WorkStatus.IN_PROGRESS,
                        "TestIssue2",
                        NOW.minusDays(LOW_PRIORITY_DAYS - 2),
                        NOW,
                        null,
                        NOW.plusDays(2),
                        WorkPriority.LOW,
                        "",
                        "This is the second test issue",
                        IssueType.FEATURE,
                        2L,
                        3L));

        requests.forEach(issueService::addIssue);
    }
}