package pl.polsl.softhouse.dto.user;

import pl.polsl.softhouse.entities.enums.UserRole;

import java.io.Serializable;

public class UserPostDto implements Serializable {
    private String email;
    private String password;
    private String firstName;
    private Boolean active;
    private String lastName;
    private UserRole role;
    private String telNum;

    public UserPostDto(String email, String password, Boolean active, String firstName, String lastName, UserRole role, String telNum) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.active = active;
        this.lastName = lastName;
        this.role = role;
        this.telNum = telNum;
    }

    public UserPostDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
