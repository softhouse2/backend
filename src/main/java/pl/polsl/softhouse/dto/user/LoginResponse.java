package pl.polsl.softhouse.dto.user;

public class LoginResponse {
    private UserGetDto user;
    private String token;

    public LoginResponse() {
    }

    public LoginResponse(UserGetDto user, String token) {
        this.user = user;
        this.token = token;
    }

    public UserGetDto getUser() {
        return user;
    }

    public void setUser(UserGetDto user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
