package pl.polsl.softhouse.dto.request;

import pl.polsl.softhouse.entities.enums.WorkPriority;
import pl.polsl.softhouse.entities.enums.WorkStatus;

import java.time.LocalDateTime;

public class RequestPostDto {
    private Long accountManagerId;
    private String description;
    private Long systemVersionId;
    private String result;
    private WorkStatus status;
    private String name;
    private LocalDateTime dateOpened;
    private LocalDateTime dateInProgress;
    private LocalDateTime dateClosed;
    private LocalDateTime deadline;
    private WorkPriority priority;

    public RequestPostDto() {}

    public RequestPostDto(Long accountManagerId,
                          String description,
                          Long systemVersionId,
                          WorkStatus status,
                          String name,
                          LocalDateTime dateOpened,
                          LocalDateTime dateInProgress,
                          LocalDateTime dateClosed,
                          LocalDateTime deadline,
                          WorkPriority priority,
                          String result) {

        this.accountManagerId = accountManagerId;
        this.description = description;
        this.systemVersionId = systemVersionId;
        this.status = status;
        this.name = name;
        this.dateOpened = dateOpened;
        this.dateInProgress = dateInProgress;
        this.dateClosed = dateClosed;
        this.deadline = deadline;
        this.priority = priority;
        this.result = result;
    }

    public Long getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(Long accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public WorkPriority getPriority() {
        return priority;
    }

    public void setPriority(WorkPriority priority) {
        this.priority = priority;
    }

    public WorkStatus getStatus() {
        return status;
    }

    public void setStatus(WorkStatus status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDateOpened() { return dateOpened; }

    public void setDateOpened(LocalDateTime dateOpened) { this.dateOpened = dateOpened; }

    public LocalDateTime getDateInProgress() { return dateInProgress; }

    public void setDateInProgress(LocalDateTime dateInProgress) { this.dateInProgress = dateInProgress; }

    public LocalDateTime getDateClosed() { return dateClosed; }

    public void setDateClosed(LocalDateTime dateClosed) { this.dateClosed = dateClosed; }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Long getSystemVersionId() {
        return systemVersionId;
    }

    public void setSystemVersionId(Long systemVersionId) {
        this.systemVersionId = systemVersionId;
    }
}
