package pl.polsl.softhouse.dto.request;


import pl.polsl.softhouse.dto.systemVersion.SystemVersionGetDto;
import pl.polsl.softhouse.dto.user.UserGetDto;
import pl.polsl.softhouse.entities.enums.WorkPriority;
import pl.polsl.softhouse.entities.enums.WorkStatus;

import java.time.LocalDateTime;
import java.util.List;

public class RequestGetDto {

    private Long id;
    private UserGetDto accountManager;
    private String description;
    private SystemVersionGetDto systemVersion;
    private String result;
    private WorkPriority priority;
    private String name;
    private WorkStatus status;
    private LocalDateTime dateOpened;
    private LocalDateTime dateInProgress;
    private LocalDateTime dateClosed;
    private LocalDateTime deadline;
    private List<Long> issuesId;
    private List<Long> allowedUsers;

    public List<Long> getAllowedUsers() {
        return allowedUsers;
    }

    public void setAllowedUsers(List<Long> allowedUsers) {
        this.allowedUsers = allowedUsers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserGetDto getAccountManager() {
        return accountManager;
    }

    public void setAccountManager(UserGetDto accountManager) {
        this.accountManager = accountManager;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public WorkStatus getStatus() {
        return status;
    }

    public void setStatus(WorkStatus status) {
        this.status = status;
    }

    public LocalDateTime getDateOpened() {
        return dateOpened;
    }

    public void setDateOpened(LocalDateTime dateOpened) {
        this.dateOpened = dateOpened;
    }

    public LocalDateTime getDateInProgress() {
        return dateInProgress;
    }

    public void setDateInProgress(LocalDateTime dateInProgress) {
        this.dateInProgress = dateInProgress;
    }

    public LocalDateTime getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(LocalDateTime dateClosed) {
        this.dateClosed = dateClosed;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public WorkPriority getPriority() {
        return priority;
    }

    public void setPriority(WorkPriority priority) {
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getIssuesId() {
        return issuesId;
    }

    public void setIssuesId(List<Long> issuesId) {
        this.issuesId = issuesId;
    }

    public void setSystemVersion(SystemVersionGetDto systemVersion) {
        this.systemVersion = systemVersion;
    }

    public SystemVersionGetDto getSystemVersion() {
        return systemVersion;
    }
}
