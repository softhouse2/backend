package pl.polsl.softhouse.dto.request;

import org.mapstruct.*;
import pl.polsl.softhouse.dto.systemVersion.SystemVersionMapper;
import pl.polsl.softhouse.dto.user.UserMapper;
import pl.polsl.softhouse.entities.*;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {UserMapper.class, SystemVersionMapper.class})
public interface RequestMapper {

    @Mapping(source = "issues", target = "issuesId", qualifiedByName = "issueToId")
    @Mapping(source = "request", target = "allowedUsers", qualifiedByName = "mapAllowedUsers")
    RequestGetDto requestToRequestDto(Request request);

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "accountManagerId", target = "accountManager")
    @Mapping(source = "systemVersionId", target = "systemVersion")
    Request updateRequest(RequestPostDto requestDto, @MappingTarget Request request);

    @Mapping(target = "id")
    UserEntity getUserFromId(Long id);

    SystemVersion getSystemVersionFromId(Long id);

    @Named("issueToId")
    static Long issueToId(Issue issue) {
        return issue.getId();
    }

    @Mapping(target = "id", expression = "java(null)")
    @Mapping(source = "accountManagerId", target = "accountManager.id")
    @Mapping(source = "systemVersionId", target = "systemVersion.id")
    @Mapping(target = "result", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "status", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "priority", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    Request createRequestFromDto(RequestPostDto requestPostDto);

    @Named("mapAllowedUsers")
    static List<Long> mapAllowedUsers(Request request) {
        List<Long> ids = new ArrayList<>();

        ids.add(request.getAccountManager().getId());

        return ids;
    }
}
