package pl.polsl.softhouse.dto.issue;

import org.mapstruct.*;
import pl.polsl.softhouse.dto.request.RequestMapper;
import pl.polsl.softhouse.dto.user.UserMapper;
import pl.polsl.softhouse.entities.Issue;
import pl.polsl.softhouse.entities.Request;
import pl.polsl.softhouse.entities.Task;
import pl.polsl.softhouse.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {UserMapper.class})
public interface IssueMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "productManagerId", target = "productManager")
    @Mapping(source = "requestId", target = "request")
    Issue updateIssue(IssuePostDto issueDto, @MappingTarget Issue issue);

    @Mapping(target = "id", expression = "java(null)")
    @Mapping(source = "requestId", target = "request.id")
    @Mapping(source = "productManagerId", target = "productManager.id")
    @Mapping(target = "result", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "status", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "priority", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    Issue createIssueFromPostDto(IssuePostDto issuePostDto);

    @Mapping(target = "id")
    UserEntity getUserFromId(Long id);

    @Mapping(target = "id")
    Request getRequestFromId(Long id);

    @Named("taskToId")
    static Long taskToId(Task task) {
        return task.getId();
    }

    // @Mapping(source = "productManager.id", target = "productManagerId")
    @Mapping(source = "request.id", target = "requestId")
    @Mapping(source = "tasks", target = "tasksId", qualifiedByName = "taskToId")
    @Mapping(source = "issue", target = "allowedUsers", qualifiedByName = "mapAllowedUsers")
    IssueGetDto issueToGetDto(Issue issue);

    @Named("mapAllowedUsers")
    static List<Long> mapAllowedUsers(Issue issue) {
        List<Long> ids = new ArrayList<>();

        ids.add(issue.getProductManager().getId());
        ids.addAll(RequestMapper.mapAllowedUsers(issue.getRequest()));

        return ids;
    }
}
