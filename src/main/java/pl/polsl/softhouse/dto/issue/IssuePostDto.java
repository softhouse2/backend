package pl.polsl.softhouse.dto.issue;

import pl.polsl.softhouse.entities.enums.IssueType;
import pl.polsl.softhouse.entities.enums.WorkPriority;
import pl.polsl.softhouse.entities.enums.WorkStatus;

import java.time.LocalDateTime;

public class IssuePostDto {
    private WorkStatus status;
    private String name;
    private LocalDateTime dateOpened;
    private LocalDateTime dateInProgress;
    private LocalDateTime dateClosed;
    private LocalDateTime deadline;
    private WorkPriority priority;
    private String result;
    private String description;
    private IssueType type;
    private Long requestId;
    private Long productManagerId;

    public IssuePostDto() {}

    public IssuePostDto(WorkStatus status,
                        String name,
                        LocalDateTime dateOpened,
                        LocalDateTime dateInProgress,
                        LocalDateTime dateClosed,
                        LocalDateTime deadline,
                        WorkPriority priority,
                        String result,
                        String description,
                        IssueType type,
                        Long requestId,
                        Long productManagerId) {
        this.status = status;
        this.name = name;
        this.dateOpened = dateOpened;
        this.dateInProgress = dateInProgress;
        this.dateClosed = dateClosed;
        this.deadline = deadline;
        this.priority = priority;
        this.result = result;
        this.description = description;
        this.type = type;
        this.requestId = requestId;
        this.productManagerId = productManagerId;
    }

    public WorkStatus getStatus() {
        return status;
    }

    public void setStatus(WorkStatus status) {
        this.status = status;
    }

    public LocalDateTime getDateOpened() {
        return dateOpened;
    }

    public void setDateOpened(LocalDateTime dateOpened) {
        this.dateOpened = dateOpened;
    }

    public LocalDateTime getDateInProgress() {
        return dateInProgress;
    }

    public void setDateInProgress(LocalDateTime dateInProgress) {
        this.dateInProgress = dateInProgress;
    }

    public LocalDateTime getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(LocalDateTime dateClosed) {
        this.dateClosed = dateClosed;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public WorkPriority getPriority() {
        return priority;
    }

    public void setPriority(WorkPriority priority) {
        this.priority = priority;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IssueType getType() {
        return type;
    }

    public void setType(IssueType type) {
        this.type = type;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public Long getProductManagerId() {
        return productManagerId;
    }

    public void setProductManagerId(Long productManagerId) {
        this.productManagerId = productManagerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
