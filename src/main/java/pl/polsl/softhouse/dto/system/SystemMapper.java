package pl.polsl.softhouse.dto.system;

import org.mapstruct.*;
import pl.polsl.softhouse.entities.SystemEntity;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface SystemMapper {

    SystemEntity createSystem(SystemPostDto systemDto);

    SystemGetDto systemToGetDto(SystemEntity system);

    @Mapping(target = "id", ignore = true)
    SystemEntity updateSystem(SystemPostDto systemDto, @MappingTarget SystemEntity system);
}
