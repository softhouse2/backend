package pl.polsl.softhouse.dto.system;

public class SystemPostDto {
    String name;

    public SystemPostDto(String name) {
        this.name = name;
    }

    public SystemPostDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
