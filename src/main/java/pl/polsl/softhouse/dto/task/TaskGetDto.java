package pl.polsl.softhouse.dto.task;

import pl.polsl.softhouse.dto.user.UserGetDto;
import pl.polsl.softhouse.entities.enums.TaskType;
import pl.polsl.softhouse.entities.enums.WorkPriority;
import pl.polsl.softhouse.entities.enums.WorkStatus;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public class TaskGetDto implements Serializable {
    private Long id;
    private WorkStatus status;
    private String result;
    private LocalDateTime dateOpened;
    private LocalDateTime dateInProgress;
    private LocalDateTime dateClosed;
    private LocalDateTime deadline;
    private WorkPriority priority;
    private String name;
    private TaskType type;
    private UserGetDto worker;
    private Long issueId;
    private List<Long> allowedUsers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public WorkStatus getStatus() {
        return status;
    }

    public void setStatus(WorkStatus status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public LocalDateTime getDateOpened() {
        return dateOpened;
    }

    public void setDateOpened(LocalDateTime dateOpened) {
        this.dateOpened = dateOpened;
    }

    public LocalDateTime getDateInProgress() {
        return dateInProgress;
    }

    public void setDateInProgress(LocalDateTime dateInProgress) {
        this.dateInProgress = dateInProgress;
    }

    public LocalDateTime getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(LocalDateTime dateClosed) {
        this.dateClosed = dateClosed;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public WorkPriority getPriority() {
        return priority;
    }

    public void setPriority(WorkPriority priority) {
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public UserGetDto getWorker() {
        return worker;
    }

    public void setWorker(UserGetDto worker) {
        this.worker = worker;
    }

    public Long getIssueId() {
        return issueId;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }

    public List<Long> getAllowedUsers() {
        return allowedUsers;
    }

    public void setAllowedUsers(List<Long> allowedUsers) {
        this.allowedUsers = allowedUsers;
    }
}
