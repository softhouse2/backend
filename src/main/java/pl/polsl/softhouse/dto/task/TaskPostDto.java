package pl.polsl.softhouse.dto.task;

import pl.polsl.softhouse.entities.enums.TaskType;
import pl.polsl.softhouse.entities.enums.WorkPriority;
import pl.polsl.softhouse.entities.enums.WorkStatus;

import java.io.Serializable;
import java.time.LocalDateTime;

public class TaskPostDto implements Serializable {
    private WorkStatus status;
    private String result;
    private LocalDateTime dateOpened;
    private LocalDateTime dateInProgress;
    private LocalDateTime dateClosed;
    private LocalDateTime deadline;
    private WorkPriority priority;
    private String name;
    private TaskType type;
    private Long workerId;
    private Long issueId;

    public TaskPostDto() {}

    public TaskPostDto(WorkStatus status, String result, LocalDateTime dateOpened, LocalDateTime dateInProgress, LocalDateTime dateClosed, LocalDateTime deadline, WorkPriority priority, String name, TaskType type, Long workerId, Long issueId) {
        this.status = status;
        this.result = result;
        this.dateOpened = dateOpened;
        this.dateInProgress = dateInProgress;
        this.dateClosed = dateClosed;
        this.deadline = deadline;
        this.priority = priority;
        this.name = name;
        this.type = type;
        this.workerId = workerId;
        this.issueId = issueId;
    }

    public WorkStatus getStatus() {
        return status;
    }

    public void setStatus(WorkStatus status) {
        this.status = status;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public LocalDateTime getDateOpened() {
        return dateOpened;
    }

    public void setDateOpened(LocalDateTime dateOpened) {
        this.dateOpened = dateOpened;
    }

    public LocalDateTime getDateInProgress() {
        return dateInProgress;
    }

    public void setDateInProgress(LocalDateTime dateInProgress) {
        this.dateInProgress = dateInProgress;
    }

    public LocalDateTime getDateClosed() {
        return dateClosed;
    }

    public void setDateClosed(LocalDateTime dateClosed) {
        this.dateClosed = dateClosed;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public WorkPriority getPriority() {
        return priority;
    }

    public void setPriority(WorkPriority priority) {
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public Long getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Long workerId) {
        this.workerId = workerId;
    }

    public Long getIssueId() {
        return issueId;
    }

    public void setIssueId(Long issueId) {
        this.issueId = issueId;
    }
}
