package pl.polsl.softhouse.dto.task;

import org.mapstruct.*;
import pl.polsl.softhouse.dto.issue.IssueMapper;
import pl.polsl.softhouse.entities.Issue;
import pl.polsl.softhouse.entities.Task;
import pl.polsl.softhouse.entities.UserEntity;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TaskMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(source = "workerId", target = "worker")
    @Mapping(source = "issueId", target = "issue")
    Task updateTask(TaskPostDto taskDto, @MappingTarget Task task);

    @Mapping(target = "id", expression = "java(null)")
    @Mapping(source = "issueId", target = "issue.id")
    @Mapping(source = "workerId", target = "worker.id")
    @Mapping(target = "result", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "status", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    @Mapping(target = "priority", nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    Task createTaskFromPostDto(TaskPostDto taskPostDto);

    @Mapping(target = "id")
    UserEntity getUserFromId(Long id);

    @Mapping(target = "id")
    Issue getIssueFromId(Long id);

    // @Mapping(source = "worker.id", target = "workerId")
    @Mapping(source = "issue.id", target = "issueId")
    @Mapping(source = "task", target = "allowedUsers", qualifiedByName = "mapAllowedUsers")
    TaskGetDto taskToGetDto(Task task);

    @Named("mapAllowedUsers")
    static List<Long> mapAllowedUsers(Task task) {
        List<Long> ids = new ArrayList<>();

        ids.add(task.getWorker().getId());
        ids.addAll(IssueMapper.mapAllowedUsers(task.getIssue()));

        return ids;
    }
}
