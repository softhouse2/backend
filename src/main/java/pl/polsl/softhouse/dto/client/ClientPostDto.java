package pl.polsl.softhouse.dto.client;

public class ClientPostDto {
    private String name;

    public ClientPostDto() {
    }

    public ClientPostDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
