package pl.polsl.softhouse.dto.client;

import org.mapstruct.*;
import pl.polsl.softhouse.entities.Client;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ClientMapper {

    Client createClientFromPostDto(ClientPostDto clientDto);

    ClientGetDto clientToGetDto(Client client);

    @Mapping(target = "id", ignore = true)
    Client updateClient(ClientPostDto clientDto, @MappingTarget Client client);
}
