package pl.polsl.softhouse.dto.systemVersion;

public class SystemVersionPostDto {
    private Long clientId;
    private Long systemId;
    private String version;

    public SystemVersionPostDto() {
    }

    public SystemVersionPostDto(Long systemId, Long clientId, String version) {
        this.clientId = clientId;
        this.systemId = systemId;
        this.version = version;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getSystemId() {
        return systemId;
    }

    public void setSystemId(Long systemId) {
        this.systemId = systemId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
