package pl.polsl.softhouse.dto.systemVersion;

import org.mapstruct.*;
import pl.polsl.softhouse.dto.client.ClientMapper;
import pl.polsl.softhouse.dto.system.SystemMapper;
import pl.polsl.softhouse.entities.SystemVersion;

@Mapper(componentModel = "spring",
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = {SystemMapper.class, ClientMapper.class})
public interface SystemVersionMapper {

    @Mapping(source = "systemId", target = "system.id")
    @Mapping(source = "clientId", target = "client.id")
    SystemVersion createSystemVersion(SystemVersionPostDto dto);

    SystemVersionGetDto systemVersionToDto(SystemVersion systemVersion);

    @Mapping(target="id", ignore = true)
    @Mapping(source = "dto.version", target = "version")
    SystemVersion updateSystemVersion(SystemVersionPostDto dto, @MappingTarget SystemVersion systemVersion);
}
