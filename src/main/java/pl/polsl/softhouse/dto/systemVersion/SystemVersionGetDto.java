package pl.polsl.softhouse.dto.systemVersion;

import pl.polsl.softhouse.dto.client.ClientGetDto;
import pl.polsl.softhouse.dto.system.SystemGetDto;

public class SystemVersionGetDto {
    private long id;
    private ClientGetDto client;
    private SystemGetDto system;
    private String version;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SystemGetDto getSystem() {
        return system;
    }

    public void setSystem(SystemGetDto system) {
        this.system = system;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public ClientGetDto getClient() {
        return client;
    }

    public void setClient(ClientGetDto client) {
        this.client = client;
    }
}
