package pl.polsl.softhouse.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.polsl.softhouse.components.security.SecurityRole;
import pl.polsl.softhouse.config.OpenApiConfig;
import pl.polsl.softhouse.config.WebConfig;
import pl.polsl.softhouse.dto.task.TaskGetDto;
import pl.polsl.softhouse.dto.task.TaskPostDto;
import pl.polsl.softhouse.entities.enums.SortingOption;
import pl.polsl.softhouse.entities.enums.WorkStatus;
import pl.polsl.softhouse.services.TaskService;

import java.time.LocalDateTime;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "api/tasks")
public class TaskController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER, SecurityRole.PRODUCT_MANAGER, SecurityRole.WORKER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<List<TaskGetDto>> getTasks(
            @RequestParam(required = false) Long userId,
            @RequestParam(required = false) WorkStatus status,
            @RequestParam(required = false) @DateTimeFormat(pattern = WebConfig.dateTimeFormat) LocalDateTime date,
            @RequestParam(required = false) Long issueId,
            @RequestParam(required = false) SortingOption sortBy
    ) {
        return ResponseEntity.ok().body(userId == null & status == null & date == null & issueId == null & sortBy == null ?
                taskService.getAllTasks() :
                taskService.getTaskByUserIdAndStatusAndDate(userId, status, date, issueId, sortBy));
    }

    @GetMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER, SecurityRole.PRODUCT_MANAGER, SecurityRole.WORKER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<TaskGetDto> getTaskById(@PathVariable Long id) {
        return ResponseEntity.ok().body(taskService.getTaskById(id));
    }

    @PostMapping
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER, SecurityRole.PRODUCT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> addTask(@RequestBody TaskPostDto taskPostDto) {
        taskService.addTask(taskPostDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER, SecurityRole.PRODUCT_MANAGER, SecurityRole.WORKER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> updateTask(@PathVariable Long id, @RequestBody TaskPostDto taskPostDto) {
        taskService.updateTask(id, taskPostDto);
        return ResponseEntity.ok().build();
    }
}
