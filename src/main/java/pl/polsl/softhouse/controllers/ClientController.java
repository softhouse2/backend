package pl.polsl.softhouse.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.polsl.softhouse.components.security.SecurityRole;
import pl.polsl.softhouse.config.OpenApiConfig;
import pl.polsl.softhouse.dto.client.ClientGetDto;
import pl.polsl.softhouse.dto.client.ClientPostDto;
import pl.polsl.softhouse.services.ClientService;

import java.util.List;

@RestController
@RequestMapping(path = "api/clients")
@CrossOrigin
public class ClientController {

    private final ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping()
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<List<ClientGetDto>> getClients(@RequestParam(required = false) Long systemId) {
        List<ClientGetDto> clients = systemId != null ?
                clientService.getClientsWithSystem(systemId) :
                clientService.getAllClients();

        return ResponseEntity.ok(clients);
    }

    @GetMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<ClientGetDto> getClientById(@PathVariable long id) {
        return ResponseEntity.ok(clientService.getClientById(id));
    }

    @PostMapping
    @Secured({SecurityRole.ADMIN})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> addClient(@RequestBody ClientPostDto clientDto) {
        clientService.addClient(clientDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> updateClient(@PathVariable Long id, @RequestBody ClientPostDto clientDto) {
        clientService.updateClient(id, clientDto);
        return ResponseEntity.ok().build();
    }
}
