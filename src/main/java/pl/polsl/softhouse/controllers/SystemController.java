package pl.polsl.softhouse.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.polsl.softhouse.components.security.SecurityRole;
import pl.polsl.softhouse.config.OpenApiConfig;
import pl.polsl.softhouse.dto.system.SystemGetDto;
import pl.polsl.softhouse.dto.system.SystemPostDto;
import pl.polsl.softhouse.services.SystemService;

import java.util.List;

@RestController
@RequestMapping(path = "api/systems")
@CrossOrigin
public class SystemController {

    private final SystemService systemService;

    public SystemController(SystemService systemService) {
        this.systemService = systemService;
    }

    @GetMapping
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<List<SystemGetDto>> getSystems() {
        return ResponseEntity.ok(systemService.getAllSystems());
    }

    @GetMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<SystemGetDto> getSystemById(@PathVariable Long id) {
        return ResponseEntity.ok(systemService.getSystemById(id));
    }

    @PostMapping
    @Secured({SecurityRole.ADMIN})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> addSystem(@RequestBody SystemPostDto systemDto) {
        systemService.addSystem(systemDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> updateSystem(@PathVariable Long id, @RequestBody SystemPostDto systemDto) {
        systemService.updateSystem(id, systemDto);
        return ResponseEntity.ok().build();
    }
}
