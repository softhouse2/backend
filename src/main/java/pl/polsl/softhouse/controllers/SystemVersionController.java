package pl.polsl.softhouse.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.polsl.softhouse.components.security.SecurityRole;
import pl.polsl.softhouse.config.OpenApiConfig;
import pl.polsl.softhouse.dto.systemVersion.SystemVersionGetDto;
import pl.polsl.softhouse.dto.systemVersion.SystemVersionPostDto;
import pl.polsl.softhouse.services.SystemVersionService;

import java.util.List;

@RestController
@RequestMapping(path = "api/systemVersions")
@CrossOrigin
public class SystemVersionController {

    private final SystemVersionService systemVersionService;

    public SystemVersionController(SystemVersionService systemVersionService) {
        this.systemVersionService = systemVersionService;
    }

    @GetMapping
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<List<SystemVersionGetDto>> getSystemVersions(
            @RequestParam(required = false) Long systemId,
            @RequestParam(required = false) Long clientId) {
        return ResponseEntity.ok(systemVersionService.getSystemVersion(systemId, clientId));
    }

    @GetMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<SystemVersionGetDto> getSystemVersionById(@PathVariable Long id) {
        return ResponseEntity.ok(systemVersionService.getSystemVersionById(id));
    }

    @PostMapping
    @Secured({SecurityRole.ADMIN})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> addSystemVersion(@RequestBody SystemVersionPostDto dto) {
        systemVersionService.addSystemVersion(dto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> updateSystemVersion(@PathVariable long id, @RequestBody SystemVersionPostDto dto) {
        systemVersionService.updateSystemVersion(id, dto);
        return ResponseEntity.ok().build();
    }
}
