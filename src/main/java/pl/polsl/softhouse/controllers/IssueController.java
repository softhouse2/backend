package pl.polsl.softhouse.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.polsl.softhouse.components.security.SecurityRole;
import pl.polsl.softhouse.config.OpenApiConfig;
import pl.polsl.softhouse.config.WebConfig;
import pl.polsl.softhouse.dto.issue.IssueGetDto;
import pl.polsl.softhouse.dto.issue.IssuePostDto;
import pl.polsl.softhouse.entities.enums.SortingOption;
import pl.polsl.softhouse.entities.enums.WorkStatus;
import pl.polsl.softhouse.services.IssueService;

import java.time.LocalDateTime;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "api/issues")
public class IssueController {

    private final IssueService issueService;

    IssueController(IssueService issueService) {
        this.issueService = issueService;
    }

    @GetMapping
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER, SecurityRole.PRODUCT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<List<IssueGetDto>> getIssues(
            @RequestParam(required = false) Long userId,
            @RequestParam(required = false) WorkStatus status,
            @RequestParam(required = false) @DateTimeFormat(pattern = WebConfig.dateTimeFormat) LocalDateTime date,
            @RequestParam(required = false) Long requestId,
            @RequestParam(required = false) SortingOption sortBy) {

        return ResponseEntity.ok().body(userId == null & status == null & date == null & requestId == null & sortBy == null ?
                issueService.getAllIssues() :
                issueService.getIssuesByUserIdAndStatusAndDateAndRequestId(userId, status, date, requestId, sortBy));
    }

    @GetMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER, SecurityRole.PRODUCT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<IssueGetDto> getIssueById(@PathVariable Long id) {
        return ResponseEntity.ok().body(issueService.getIssueById(id));
    }

    @PostMapping
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> addIssue(@RequestBody IssuePostDto issuePostDto) {
        issueService.addIssue(issuePostDto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER, SecurityRole.PRODUCT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> updateIssue(@PathVariable Long id, @RequestBody IssuePostDto issuePostDto) {
        issueService.updateIssue(id, issuePostDto);
        return ResponseEntity.ok().build();
    }
}
