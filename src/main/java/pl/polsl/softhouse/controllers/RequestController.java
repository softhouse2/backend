package pl.polsl.softhouse.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.polsl.softhouse.components.security.SecurityRole;
import pl.polsl.softhouse.config.OpenApiConfig;
import pl.polsl.softhouse.config.WebConfig;
import pl.polsl.softhouse.dto.request.RequestGetDto;
import pl.polsl.softhouse.dto.request.RequestPostDto;
import pl.polsl.softhouse.entities.enums.SortingOption;
import pl.polsl.softhouse.entities.enums.WorkStatus;
import pl.polsl.softhouse.services.RequestService;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "api/requests")
public class RequestController {

    private final RequestService requestService;

    public RequestController(RequestService requestService) {
        this.requestService = requestService;
    }

    @GetMapping
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<List<RequestGetDto>> getAllRequests(
            @RequestParam(required = false) Long userId,
            @RequestParam(required = false) WorkStatus status,
            @RequestParam(required = false) @DateTimeFormat(pattern = WebConfig.dateTimeFormat) LocalDateTime date,
            @RequestParam(required = false) SortingOption sortBy) {

        return ResponseEntity.ok().body(userId == null & status == null & date == null & sortBy == null
                ? requestService.getAllRequests()
                : requestService.getAllRequestsByUserIdAndStatusAndDate(userId, status, date, sortBy));
    }

    @GetMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<RequestGetDto> getRequestById(@PathVariable Long id) {
        return ResponseEntity.ok().body(requestService.getRequestById(id));
    }

    @PostMapping
    @Secured({SecurityRole.ADMIN})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> addRequest(@RequestBody RequestPostDto requestPostDto) {
        long id = requestService.addRequest(requestPostDto);
        URI createdUri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id)
                .toUri();

        return ResponseEntity.created(createdUri).build();
    }

    @PutMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> updateRequest(@PathVariable Long id, @RequestBody RequestPostDto requestDto) {
        requestService.updateRequest(id, requestDto);

        return ResponseEntity.ok().build();
    }
}
