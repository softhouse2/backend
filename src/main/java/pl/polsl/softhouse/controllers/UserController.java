package pl.polsl.softhouse.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pl.polsl.softhouse.components.security.SecurityRole;
import pl.polsl.softhouse.config.OpenApiConfig;
import pl.polsl.softhouse.dto.user.LoginResponse;
import pl.polsl.softhouse.dto.user.UserAuthDto;
import pl.polsl.softhouse.dto.user.UserGetDto;
import pl.polsl.softhouse.dto.user.UserPostDto;
import pl.polsl.softhouse.entities.enums.UserRole;
import pl.polsl.softhouse.services.UserService;

import java.net.URI;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(path = "api/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER, SecurityRole.PRODUCT_MANAGER, SecurityRole.WORKER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<List<UserGetDto>> getUsers(@RequestParam(required = false) UserRole role) {
        List<UserGetDto> users = role != null
                ? userService.getUsersByRole(role)
                : userService.getAllUsers();
        return ResponseEntity.ok().body(users);
    }

    @GetMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER, SecurityRole.PRODUCT_MANAGER, SecurityRole.WORKER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<UserGetDto> getUserById(@PathVariable Long id) {
        return ResponseEntity.ok().body(userService.getUserById(id));
    }

    @PostMapping
    public ResponseEntity<Void> addUser(@RequestBody UserPostDto userDto) {
        long userId = userService.addUser(userDto);
        URI createdUri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(userId)
                .toUri();

        return ResponseEntity.created(createdUri).build();
    }

    @PutMapping(path = "{id}")
    @Secured({SecurityRole.ADMIN, SecurityRole.ACCOUNT_MANAGER, SecurityRole.PRODUCT_MANAGER, SecurityRole.WORKER})
    @Operation(security = @SecurityRequirement(name = OpenApiConfig.SECURITY_NAME))
    public ResponseEntity<Void> updateUser(@PathVariable Long id, @RequestBody UserPostDto userDto) {
        userService.updateUser(id, userDto);
        return ResponseEntity.ok().build();
    }

    @PostMapping(path = "login")
    public ResponseEntity<LoginResponse> authenticateUser(@RequestBody UserAuthDto authDto) {
        String jwt = userService.loginAndGetJwt(authDto.getEmail(), authDto.getPassword());
        UserGetDto userDto = userService.getUserByEmail(authDto.getEmail());

        return ResponseEntity.ok(new LoginResponse(userDto, jwt));
    }
}
