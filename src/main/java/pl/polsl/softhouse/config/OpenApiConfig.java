package pl.polsl.softhouse.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;

@Configuration
@OpenAPIDefinition(info = @Info(title = "softhouse", version = "v1"))
@SecurityScheme(
        name = OpenApiConfig.SECURITY_NAME,
        type = SecuritySchemeType.APIKEY,
        in = SecuritySchemeIn.HEADER,
        paramName = HttpHeaders.AUTHORIZATION,
        description = "Json Web Token in the format of `Bearer <token>`."
)
public class OpenApiConfig {
    public static final String SECURITY_NAME = "JWT";
}
