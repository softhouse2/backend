package pl.polsl.softhouse.services;

import org.springframework.stereotype.Service;
import pl.polsl.softhouse.components.GenericValidator;
import pl.polsl.softhouse.dto.system.SystemGetDto;
import pl.polsl.softhouse.dto.system.SystemMapper;
import pl.polsl.softhouse.dto.system.SystemPostDto;
import pl.polsl.softhouse.entities.SystemEntity;
import pl.polsl.softhouse.exceptions.InvalidDataException;
import pl.polsl.softhouse.exceptions.NotFoundException;
import pl.polsl.softhouse.repositories.SystemRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SystemService {

    private final SystemRepository systemRepository;
    private final GenericValidator<SystemEntity> validator;
    private final SystemMapper systemMapper;

    public SystemService(
            SystemRepository systemRepository,
            GenericValidator<SystemEntity> validator,
            SystemMapper systemMapper) {
        this.systemRepository = systemRepository;
        this.validator = validator;
        this.systemMapper = systemMapper;
    }

    public long addSystem(SystemPostDto systemDto) {
        if (systemDto == null) {
            throw new InvalidDataException("No data sent");
        }
        SystemEntity system = systemMapper.createSystem(systemDto);
        validator.validateOrThrow(system);
        return systemRepository.save(system).getId();
    }

    public List<SystemGetDto> getAllSystems() {
        return systemRepository.findAll()
                .stream()
                .map(systemMapper::systemToGetDto)
                .collect(Collectors.toList());
    }

    public SystemGetDto getSystemById(Long id) {
        if (id == null) {
            throw new InvalidDataException("No data sent");
        }

        return systemRepository.findById(id)
                .map(systemMapper::systemToGetDto)
                .orElseThrow(() -> new NotFoundException("System", id));
    }

    public void updateSystem(Long id, SystemPostDto systemDto) {
        if (systemDto == null) {
            throw new InvalidDataException("No data sent");
        }

        SystemEntity system = systemRepository.findById(id)
                .map(foundSystem -> systemMapper.updateSystem(systemDto, foundSystem))
                .orElseThrow(() -> new NotFoundException("System", id));
        validator.validateOrThrow(system);
        systemRepository.save(system);
    }
}
