package pl.polsl.softhouse.services;

import org.springframework.stereotype.Service;
import pl.polsl.softhouse.components.GenericValidator;
import pl.polsl.softhouse.dto.systemVersion.SystemVersionGetDto;
import pl.polsl.softhouse.dto.systemVersion.SystemVersionMapper;
import pl.polsl.softhouse.dto.systemVersion.SystemVersionPostDto;
import pl.polsl.softhouse.entities.Client;
import pl.polsl.softhouse.entities.SystemVersion;
import pl.polsl.softhouse.entities.SystemEntity;
import pl.polsl.softhouse.exceptions.InvalidDataException;
import pl.polsl.softhouse.exceptions.NotFoundException;
import pl.polsl.softhouse.repositories.ClientRepository;
import pl.polsl.softhouse.repositories.SystemVersionRepository;
import pl.polsl.softhouse.repositories.SystemRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SystemVersionService {

    private final SystemVersionRepository systemVersionRepo;
    private final SystemRepository systemRepository;
    private final ClientRepository clientRepository;
    private final SystemVersionMapper systemVersionMapper;
    private final GenericValidator<SystemVersion> validator;

    public SystemVersionService(SystemVersionRepository systemVersionRepo,
                                SystemRepository systemRepository,
                                ClientRepository clientRepository,
                                SystemVersionMapper systemVersionMapper,
                                GenericValidator<SystemVersion> validator) {

        this.systemVersionRepo = systemVersionRepo;
        this.systemRepository = systemRepository;
        this.clientRepository = clientRepository;
        this.systemVersionMapper = systemVersionMapper;
        this.validator = validator;
    }

    public void addSystemVersion(SystemVersionPostDto dto) {
        if (dto == null) {
            throw new InvalidDataException("No data sent.");
        }
        if (dto.getClientId() == null ||
            !clientRepository.existsById(dto.getClientId())) {
            throw new NotFoundException("Client", dto.getClientId());
        }
        if (dto.getClientId() == null ||
                !clientRepository.existsById(dto.getClientId())) {
            throw new NotFoundException("Client", dto.getClientId());
        }

        SystemVersion sv = systemVersionMapper.createSystemVersion(dto);
        validator.validateOrThrow(sv);
        systemVersionRepo.save(sv);
    }

    public List<SystemVersionGetDto> getSystemVersion(Long systemId, Long clientId) {
        List<SystemVersion> retList;
        if (systemId == null && clientId == null) {
            retList = systemVersionRepo.findAll();
        }
        else {
            retList = systemVersionRepo.findBySystemIdOrClientId(systemId, clientId);
        }

        return retList
                .stream()
                .map(systemVersionMapper::systemVersionToDto)
                .collect(Collectors.toList());
    }

    public SystemVersionGetDto getSystemVersionById(long id) {
        return systemVersionRepo.findById(id)
                .map(systemVersionMapper::systemVersionToDto)
                .orElseThrow(() -> new NotFoundException("SystemVersion", id));
    }

    public void updateSystemVersion(long id, SystemVersionPostDto dto) {
        if (dto == null) {
            throw new InvalidDataException("No data sent.");
        }

        SystemEntity newSystem = dto.getSystemId() == null
                ? null
                : systemRepository.findById(dto.getSystemId())
                        .orElseThrow(() -> new NotFoundException("System", dto.getSystemId()));

        Client newClient = dto.getClientId() == null
                ? null
                : clientRepository.findById(dto.getClientId())
                        .orElseThrow(() -> new NotFoundException("Client", dto.getClientId()));

        SystemVersion sv = systemVersionRepo.findById(id)
                .map(foundSv -> systemVersionMapper.updateSystemVersion(dto, foundSv))
                .orElseThrow(() -> new NotFoundException("SystemVersion", id));
        if (newSystem != null) {
            sv.setSystem(newSystem);
        }
        if (newClient != null) {
            sv.setClient(newClient);
        }

        validator.validateOrThrow(sv);
        systemVersionRepo.save(sv);
    }
}
