package pl.polsl.softhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.polsl.softhouse.components.GenericSorter;
import pl.polsl.softhouse.components.GenericValidator;
import pl.polsl.softhouse.components.statusListeners.StatusListener;
import pl.polsl.softhouse.components.security.PermissionGuard;
import pl.polsl.softhouse.dto.request.RequestGetDto;
import pl.polsl.softhouse.dto.request.RequestMapper;
import pl.polsl.softhouse.dto.request.RequestPostDto;
import pl.polsl.softhouse.entities.Request;
import pl.polsl.softhouse.entities.enums.SortingOption;
import pl.polsl.softhouse.entities.enums.UserRole;
import pl.polsl.softhouse.entities.enums.WorkStatus;
import pl.polsl.softhouse.exceptions.InvalidDataException;
import pl.polsl.softhouse.exceptions.request.RequestNotFoundException;
import pl.polsl.softhouse.repositories.RequestRepository;
import pl.polsl.softhouse.repositories.UserRepository;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RequestService extends BaseWorkService {

    private final RequestRepository requestRepository;
    private final RequestMapper requestMapper;
    private final GenericValidator<Request> validator;
    private final StatusListener statusListener;
    private final GenericSorter<Request> sorter;
    private final PermissionGuard guard;
    @Autowired
    public RequestService(RequestRepository requestRepository,
                          UserRepository userRepository,
                          RequestMapper requestMapper,
                          GenericValidator<Request> validator,
                          StatusListener statusListener,
                          GenericSorter<Request> sorter,
                          PermissionGuard guard) {

        super(userRepository);

        this.requestRepository = requestRepository;
        this.requestMapper = requestMapper;
        this.validator = validator;
        this.statusListener = statusListener;
        this.sorter = sorter;
        this.guard = guard;
    }

    public List<RequestGetDto> getAllRequests() {
        return requestRepository.findAll().stream()
                .map(requestMapper::requestToRequestDto)
                .collect(Collectors.toList());
    }

    public RequestGetDto getRequestById(Long id) {
        if (id == null) {
            throw new InvalidDataException("No id provided.");
        }

        Request request = requestRepository.findById(id)
                .orElseThrow(() -> new RequestNotFoundException(id));

        return requestMapper.requestToRequestDto(request);
    }

    public List<RequestGetDto> getAllRequestsByUserIdAndStatusAndDate(
            Long userId,
            WorkStatus status,
            LocalDateTime startDt,
            SortingOption sortBy) {

        if(userId != null)
            checkUserOrThrow(userId);

        LocalDateTime endDt = getEndDateTime(startDt);

        List<Request> fetched;
        if (startDt != null && endDt != null) {
            fetched = requestRepository
                    .findAllRequestsByUserIdAndStatusAndDate(
                            userId,
                            status,
                            startDt,
                            endDt);
        } else {
            fetched = requestRepository
                    .findAllRequestsByUserIdAndStatus(
                            userId,
                            status);
        }

        return fetched.stream()
                .sorted(sortBy != null ? sorter.getComparator(sortBy) : Comparator.comparing(Request::getId))
                .map(requestMapper::requestToRequestDto)
                .collect(Collectors.toList());
    }

    public void deleteRequestById(Long id) {
        if (id == null) {
            throw new InvalidDataException("No id provided.");
        }

        if (!requestRepository.existsById(id)) {
            throw new RequestNotFoundException(id);
        }

        requestRepository.deleteById(id);
    }

    public long addRequest(RequestPostDto requestPostDto) {
        if (requestPostDto == null) {
            throw new InvalidDataException(("No data sent."));
        }

        if (requestPostDto.getAccountManagerId() == null) {
            throw new InvalidDataException("No account manager id provided.");
        }

        checkUserOrThrow(requestPostDto.getAccountManagerId());

        Request request = requestMapper.createRequestFromDto(requestPostDto);
        validator.validateOrThrow(request);
        return requestRepository.save(request).getId();
    }

    public void updateRequest(Long id, RequestPostDto requestDto) {
        if (id == null || requestDto == null) {
            throw new InvalidDataException(("No id provided."));
        }

        if (requestDto.getAccountManagerId() != null) {
            checkUserOrThrow(requestDto.getAccountManagerId());
        }

        Request request = requestRepository
                .findById(id)
                .map((foundRequest) -> requestMapper
                        .updateRequest(requestDto, foundRequest))
                .orElseThrow(() -> new RequestNotFoundException(id));

        guard.checkRequestUpdatePermOrThrow(request);

        validator.validateOrThrow(request);
        requestRepository.save(request);
        if(requestDto.getStatus() == WorkStatus.CANCELED)
            statusListener.checkAndPropagateCancelling(request);
    }

    private void checkUserOrThrow(long accountManagerId) {
        checkUserOrThrow(accountManagerId, UserRole.ACCOUNT_MANAGER);
    }
}
