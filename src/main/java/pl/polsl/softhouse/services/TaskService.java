package pl.polsl.softhouse.services;

import org.springframework.stereotype.Service;
import pl.polsl.softhouse.components.GenericSorter;
import pl.polsl.softhouse.components.GenericValidator;
import pl.polsl.softhouse.components.statusListeners.StatusListener;
import pl.polsl.softhouse.components.security.PermissionGuard;
import pl.polsl.softhouse.dto.task.TaskGetDto;
import pl.polsl.softhouse.dto.task.TaskMapper;
import pl.polsl.softhouse.dto.task.TaskPostDto;
import pl.polsl.softhouse.entities.Issue;
import pl.polsl.softhouse.entities.Task;
import pl.polsl.softhouse.entities.enums.SortingOption;
import pl.polsl.softhouse.entities.enums.UserRole;
import pl.polsl.softhouse.entities.enums.WorkStatus;
import pl.polsl.softhouse.exceptions.InvalidDataException;
import pl.polsl.softhouse.exceptions.issue.IssueNotFoundException;
import pl.polsl.softhouse.exceptions.task.TaskNotFoundException;
import pl.polsl.softhouse.repositories.IssueRepository;
import pl.polsl.softhouse.repositories.TaskRepository;
import pl.polsl.softhouse.repositories.UserRepository;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService extends BaseWorkService {

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    private final IssueRepository issueRepository;
    private final GenericValidator<Task> validator;
    private final StatusListener statusListener;
    private final GenericSorter<Task> sorter;
    private final PermissionGuard guard;

    public TaskService(TaskRepository taskRepository,
                       TaskMapper taskMapper,
                       UserRepository userRepository,
                       IssueRepository issueRepository,
                       GenericValidator<Task> validator,
                       StatusListener statusListener,
                       GenericSorter<Task> sorter,
                       PermissionGuard guard) {

        super(userRepository);

        this.taskRepository = taskRepository;
        this.taskMapper = taskMapper;
        this.issueRepository = issueRepository;
        this.validator = validator;
        this.statusListener = statusListener;
        this.sorter = sorter;
        this.guard = guard;
    }

    public List<TaskGetDto> getAllTasks() {
        return taskRepository
                .findAll()
                .stream()
                .map(taskMapper::taskToGetDto)
                .collect(Collectors.toList());
    }

    public TaskGetDto getTaskById(Long id) {
        if (id == null)
            throw new InvalidDataException("No id provided");
        return taskRepository
                .findById(id)
                .map(taskMapper::taskToGetDto)
                .orElseThrow(() -> new TaskNotFoundException(id));
    }

    public List<TaskGetDto> getTaskByUserIdAndStatusAndDate(
            Long userId,
            WorkStatus status,
            LocalDateTime startDt,
            Long issueId,
            SortingOption sortBy) {

        if (userId != null)
            checkUserOrThrow(userId);
        if (issueId != null)
            findIssueOrThrow(issueId);

        LocalDateTime endDt = getEndDateTime(startDt);

        List<Task> fetched;
        if (startDt != null && endDt != null) {
            fetched = taskRepository
                    .findAllTasksByUserIdAndStatusAndDateAndIssueId(
                            userId,
                            status,
                            startDt,
                            endDt,
                            issueId);
        } else {
            fetched = taskRepository
                    .findAllTasksByUserIdAndStatusAndIssueId(
                            userId,
                            status,
                            issueId);
        }

        return fetched.stream()
                .sorted(sortBy != null ? sorter.getComparator(sortBy) : Comparator.comparing(Task::getId))
                .map(taskMapper::taskToGetDto)
                .collect(Collectors.toList());
    }

    public void addTask(TaskPostDto taskPostDto) {
        if (taskPostDto == null)
            throw new InvalidDataException("No task data provided");
        if (taskPostDto.getWorkerId() == null)
            throw new InvalidDataException("No worker id provided");
        if (taskPostDto.getIssueId() == null)
            throw new InvalidDataException("No issue id provided");

        checkUserOrThrow(taskPostDto.getWorkerId());
        Issue issue = findIssueOrThrow(taskPostDto.getIssueId());

        Task task = taskMapper.createTaskFromPostDto(taskPostDto);
        validator.validateOrThrow(task);
        if (issue.getStatus() == WorkStatus.CANCELED)
            task.setStatus(WorkStatus.CANCELED);
        if(taskPostDto.getStatus() == WorkStatus.IN_PROGRESS)
            statusListener.checkAndPropagateInProgress(task);
        taskRepository.save(task);
    }

    public void updateTask(Long id, TaskPostDto taskPostDto) {
        if (id == null)
            throw new InvalidDataException("No id provided");
        if (taskPostDto == null)
            throw new InvalidDataException("No data sent");
        if (taskPostDto.getWorkerId() != null)
            checkUserOrThrow(taskPostDto.getWorkerId());
        if (taskPostDto.getIssueId() != null)
            findIssueOrThrow(taskPostDto.getIssueId());

        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException(id));

        guard.checkTaskChangePermOrThrow(task);

        task = taskMapper.updateTask(taskPostDto, task);
        validator.validateOrThrow(task);
        taskRepository.save(task);
        if (taskPostDto.getStatus() == WorkStatus.IN_PROGRESS)
            statusListener.checkAndPropagateInProgress(task);
    }

    private void checkUserOrThrow(long userId) {
        checkUserOrThrow(userId, UserRole.WORKER);
    }

    private Issue findIssueOrThrow(long issueId) {
        return issueRepository.findById(issueId)
                .orElseThrow(() -> new IssueNotFoundException(issueId));
    }
}
