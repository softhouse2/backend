package pl.polsl.softhouse.services;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.polsl.softhouse.components.GenericValidator;
import pl.polsl.softhouse.components.security.JwtUtil;
import pl.polsl.softhouse.components.security.PermissionGuard;
import pl.polsl.softhouse.dto.user.UserGetDto;
import pl.polsl.softhouse.dto.user.UserMapper;
import pl.polsl.softhouse.dto.user.UserPostDto;
import pl.polsl.softhouse.entities.UserEntity;
import pl.polsl.softhouse.entities.enums.UserRole;
import pl.polsl.softhouse.exceptions.AuthenticationException;
import pl.polsl.softhouse.exceptions.InvalidDataException;
import pl.polsl.softhouse.exceptions.user.UserAlreadyExistsException;
import pl.polsl.softhouse.exceptions.user.UserNotFoundException;
import pl.polsl.softhouse.repositories.UserRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final GenericValidator<UserEntity> validator;
    private final PasswordEncoder passwordEncoder;
    private final PermissionGuard guard;
    private final AuthenticationManager authManager;
    private final JwtUtil jwtUtil;

    public UserService(UserRepository userRepository,
                       UserMapper userMapper,
                       GenericValidator<UserEntity> validator,
                       AuthenticationManager authManager,
                       PasswordEncoder passwordEncoder,
                       PermissionGuard guard,
                       JwtUtil jwtUtil) {

        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.validator = validator;
        this.authManager = authManager;
        this.passwordEncoder = passwordEncoder;
        this.guard = guard;
        this.jwtUtil = jwtUtil;
    }

    public List<UserGetDto> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(userMapper::userToGetDto)
                .collect(Collectors.toList());
    }

    public List<UserGetDto> getUsersByRole(UserRole role) {
        if(role == null)
            throw new InvalidDataException("No role provided");
        return userRepository.findByRole(role)
                .stream()
                .map(userMapper::userToGetDto)
                .collect(Collectors.toList());
    }

    public UserGetDto getUserById(Long id) {
        if (id == null) {
            throw new InvalidDataException("No ID provided.");
        }

        return userRepository.findById(id)
                .map(userMapper::userToGetDto)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    public long addUser(UserPostDto userDto) {
        if (userDto == null) {
            throw new InvalidDataException("No data sent.");
        }

        if (!checkIfUsernameIsUnique(userDto.getEmail())) {
            throw UserAlreadyExistsException.fromUsername(userDto.getEmail());
        }

        UserEntity user = userMapper.createUserFromDto(userDto);

        if (isRawPassInvalid(user.getPassword())) {
            throw new InvalidDataException("Invalid password");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        validator.validateOrThrow(user);

        return userRepository.save(user).getId();
    }

    public void deleteUser(Long id) {
        if (id == null) {
            throw new InvalidDataException("No ID provided.");
        }

        if (!userRepository.existsById(id)) {
            throw new UserNotFoundException(id);
        }

        userRepository.deleteById(id);
    }

    public void updateUser(Long id, UserPostDto userDto) {
        if (id == null || userDto == null) {
            throw new InvalidDataException("No data sent.");
        }

        UserEntity user = userRepository.findById(id)
                .map(foundUser -> userMapper.updateUser(userDto, foundUser))
                .orElseThrow(() -> new UserNotFoundException(id));

        if(userDto.getPassword() != null && !userDto.getPassword().isBlank()) {
            if (isRawPassInvalid(user.getPassword()))
                throw new InvalidDataException("Invalid password");

            guard.checkPasswordUpdatePermOrThrow();
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        }
        validator.validateOrThrow(user);

        userRepository.save(user);
    }

    public String loginAndGetJwt(String username, String password) {
        if (username == null || username.isBlank() ||
                password == null || password.isBlank()) {
            throw new InvalidDataException("No data sent.");
        }

        try {
            Authentication auth = authManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, password)
            );
            User user = (User) auth.getPrincipal();

            return jwtUtil.generateToken(user.getUsername());
        } catch (BadCredentialsException | InternalAuthenticationServiceException e) {
            throw new AuthenticationException(e.getMessage());
        }
    }

    public UserGetDto getUserByEmail(String email) {
        if (email == null) {
            throw new InvalidDataException("No data sent.");
        }

        return userRepository.findByEmail(email)
                .map(userMapper::userToGetDto)
                .orElseThrow(() -> UserNotFoundException.fromUsername(email));
    }

    private boolean checkIfUsernameIsUnique(String username) {
        return userRepository.findByEmail(username).isEmpty();
    }

    private boolean isRawPassInvalid(String password) {
        final int minLen = 4;
        final int maxLen = 24;
        final String charWhitelist = "[A-Za-z\\d!@#$%^&*\\-_]";
        final String lengthSpecifier = String.format("{%d,%d}", minLen, maxLen);

        return password == null ||
                !password.matches("^" + charWhitelist + lengthSpecifier + "$");
    }
}
