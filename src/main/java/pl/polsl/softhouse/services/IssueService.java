package pl.polsl.softhouse.services;

import org.springframework.stereotype.Service;
import pl.polsl.softhouse.components.GenericSorter;
import pl.polsl.softhouse.components.GenericValidator;
import pl.polsl.softhouse.components.statusListeners.StatusListener;
import pl.polsl.softhouse.components.security.PermissionGuard;
import pl.polsl.softhouse.dto.issue.IssueGetDto;
import pl.polsl.softhouse.dto.issue.IssueMapper;
import pl.polsl.softhouse.dto.issue.IssuePostDto;
import pl.polsl.softhouse.entities.Issue;
import pl.polsl.softhouse.entities.Request;
import pl.polsl.softhouse.entities.enums.SortingOption;
import pl.polsl.softhouse.entities.enums.UserRole;
import pl.polsl.softhouse.entities.enums.WorkStatus;
import pl.polsl.softhouse.exceptions.InvalidDataException;
import pl.polsl.softhouse.exceptions.issue.IssueNotFoundException;
import pl.polsl.softhouse.exceptions.request.RequestNotFoundException;
import pl.polsl.softhouse.repositories.IssueRepository;
import pl.polsl.softhouse.repositories.RequestRepository;
import pl.polsl.softhouse.repositories.UserRepository;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IssueService extends BaseWorkService {
    private final IssueRepository issueRepository;
    private final IssueMapper issueMapper;
    private final RequestRepository requestRepository;
    private final GenericValidator<Issue> validator;
    private final StatusListener statusListener;
    private final GenericSorter<Issue> sorter;
    private final PermissionGuard guard;

    public IssueService(IssueRepository issueRepository,
            IssueMapper issueMapper,
            UserRepository userRepository,
            RequestRepository requestRepository,
            GenericValidator<Issue> validator,
            StatusListener statusListener,
            GenericSorter<Issue> sorter,
            PermissionGuard guard) {
        super(userRepository);

        this.issueRepository = issueRepository;
        this.issueMapper = issueMapper;
        this.requestRepository = requestRepository;
        this.validator = validator;
        this.statusListener = statusListener;
        this.sorter = sorter;
        this.guard = guard;
    }

    public List<IssueGetDto> getAllIssues() {
        return issueRepository.findAll()
                .stream()
                .map(issueMapper::issueToGetDto)
                .collect(Collectors.toList());
    }

    public IssueGetDto getIssueById(Long id) {
        if (id == null)
            throw new InvalidDataException("No id provided.");

        return issueRepository
                .findById(id)
                .map(issueMapper::issueToGetDto)
                .orElseThrow(() -> new IssueNotFoundException(id));
    }

    public void updateIssue(Long id, IssuePostDto issueDto) {
        if (id == null)
            throw new InvalidDataException("No id provided.");
        if (issueDto == null)
            throw new InvalidDataException("No data sent");
        if (issueDto.getProductManagerId() != null)
            checkUserOrThrow(issueDto.getProductManagerId());
        if (issueDto.getRequestId() != null)
            findRequestOrThrow(issueDto.getRequestId());

        Issue issue = issueRepository
                .findById(id)
                .orElseThrow(() -> new IssueNotFoundException(id));

        guard.checkIssueUpdatePermOrThrow(issue);

        issue = issueMapper.updateIssue(issueDto, issue);
        validator.validateOrThrow(issue);
        issueRepository.save(issue);
        if (issueDto.getStatus() != null) {
            switch (issueDto.getStatus()) {
                case CANCELED:
                    statusListener.checkAndPropagateCancelling(issue);
                    break;
                case IN_PROGRESS:
                    statusListener.checkAndPropagateInProgress(issue);
                    break;
                default:
                    break;
            }
        }
    }

    public void addIssue(IssuePostDto issuePostDto) {
        if (issuePostDto == null)
            throw new InvalidDataException("No data provided");
        if (issuePostDto.getProductManagerId() == null)
            throw new InvalidDataException("No product manager id provided");
        if (issuePostDto.getRequestId() == null)
            throw new InvalidDataException("No request id provided");

        checkUserOrThrow(issuePostDto.getProductManagerId());
        Request request = findRequestOrThrow(issuePostDto.getRequestId());

        Issue issue = issueMapper.createIssueFromPostDto(issuePostDto);
        validator.validateOrThrow(issue);

        if (request.getStatus() == WorkStatus.CANCELED)
            issue.setStatus(WorkStatus.CANCELED);

        if(issuePostDto.getStatus() == WorkStatus.IN_PROGRESS)
            statusListener.checkAndPropagateInProgress(issue);
        issueRepository.save(issue);
    }

    public List<IssueGetDto> getIssuesByUserIdAndStatusAndDateAndRequestId(
            Long userId,
            WorkStatus status,
            LocalDateTime startDt,
            Long requestId,
            SortingOption sortBy) {

        LocalDateTime endDt = getEndDateTime(startDt);

        List<Issue> fetched;
        if (startDt != null && endDt != null) {
            fetched = issueRepository
                    .findAllIssuesByUserIdAndStatusAndDateAndRequestId(
                            userId,
                            status,
                            startDt,
                            endDt,
                            requestId);
        } else {
            fetched = issueRepository
                    .findAllIssuesByUserIdAndStatusAndRequestId(
                            userId,
                            status,
                            requestId);
        }

        return fetched.stream()
                .sorted(sortBy != null ? sorter.getComparator(sortBy) : Comparator.comparing(Issue::getId))
                .map(issueMapper::issueToGetDto)
                .collect(Collectors.toList());
    }

    private Request findRequestOrThrow(long requestId) {
        return requestRepository.findById(requestId)
                .orElseThrow(() -> new RequestNotFoundException(requestId));
    }

    private void checkUserOrThrow(long productManagerId) {
        checkUserOrThrow(productManagerId, UserRole.PRODUCT_MANAGER);
    }
}
