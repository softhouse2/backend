package pl.polsl.softhouse.services;

import pl.polsl.softhouse.entities.UserEntity;
import pl.polsl.softhouse.entities.enums.UserRole;
import pl.polsl.softhouse.exceptions.AuthorizationException;
import pl.polsl.softhouse.exceptions.BadRequestException;
import pl.polsl.softhouse.exceptions.user.UserNotFoundException;
import pl.polsl.softhouse.repositories.UserRepository;

import java.time.LocalDateTime;

public abstract class BaseWorkService {

    protected final UserRepository userRepository;

    public BaseWorkService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    protected void checkUserOrThrow(long userId, UserRole authorizedRole) {
        UserEntity user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException(userId));

        if (!user.getActive()) {
            throw new BadRequestException("This user is inactive");
        }

        if (user.getRole() != authorizedRole) {
            throw new AuthorizationException("The member (role: " + user.getRole() +  ") cannot be assigned to this work unit (required: " + authorizedRole + ")");
        }
    }

    protected void checkIfUserExists(long userId) {
        userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
    }

    protected LocalDateTime getEndDateTime(LocalDateTime start) {
        return start != null ? start.plusDays(1) : null;
    }
}
