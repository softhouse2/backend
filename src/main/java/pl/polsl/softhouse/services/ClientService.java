package pl.polsl.softhouse.services;

import org.springframework.stereotype.Service;
import pl.polsl.softhouse.components.GenericValidator;
import pl.polsl.softhouse.dto.client.ClientGetDto;
import pl.polsl.softhouse.dto.client.ClientMapper;
import pl.polsl.softhouse.dto.client.ClientPostDto;
import pl.polsl.softhouse.entities.Client;
import pl.polsl.softhouse.entities.SystemEntity;
import pl.polsl.softhouse.exceptions.InvalidDataException;
import pl.polsl.softhouse.exceptions.NotFoundException;
import pl.polsl.softhouse.repositories.ClientRepository;
import pl.polsl.softhouse.repositories.SystemRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {

    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;
    private final GenericValidator<Client> validator;
    private final SystemRepository systemRepository;

    public ClientService(
            ClientRepository clientRepository,
            ClientMapper clientMapper,
            GenericValidator<Client> validator,
            SystemRepository systemRepository) {

        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
        this.validator = validator;
        this.systemRepository = systemRepository;
    }

    public long addClient(ClientPostDto clientDto) {
        if (clientDto == null) {
            throw new InvalidDataException("No data sent.");
        }

        Client client = clientMapper.createClientFromPostDto(clientDto);
        validator.validateOrThrow(client);
        return clientRepository.save(client).getId();
    }

    public List<ClientGetDto> getAllClients() {
        return clientRepository.findAll()
                .stream()
                .map(clientMapper::clientToGetDto)
                .collect(Collectors.toList());
    }

    public ClientGetDto getClientById(Long id) {
        if (id == null){
            throw new InvalidDataException("No id provided.");
        }
        return clientRepository.findById(id)
                .map(clientMapper::clientToGetDto)
                .orElseThrow(() -> new NotFoundException("Client", id));
    }

    public void updateClient(long id, ClientPostDto clientDto) {
        if (clientDto == null) {
            throw new InvalidDataException("No data sent.");
        }

        Client client = clientRepository.findById(id)
                .map(foundClient -> clientMapper.updateClient(clientDto, foundClient))
                .orElseThrow(() -> new NotFoundException("Client", id));
        validator.validateOrThrow(client);
        clientRepository.save(client);
    }

    public List<ClientGetDto> getClientsWithSystem(long systemId) {
        SystemEntity foundSystem = systemRepository.findById(systemId)
                .orElseThrow(() -> new NotFoundException("System", systemId));

        return clientRepository.findBySystemId(foundSystem.getId())
                .stream()
                .map(clientMapper::clientToGetDto)
                .collect(Collectors.toList());
    }
}
