package pl.polsl.softhouse.exceptions.task;

import pl.polsl.softhouse.exceptions.NotFoundException;

public class TaskNotFoundException extends NotFoundException {
    public TaskNotFoundException(String message) {
        super(message);
    }
    public TaskNotFoundException(long id) {
        super("Task", id);
    }
}
