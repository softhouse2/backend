package pl.polsl.softhouse.exceptions;

public class InvalidDataException extends BadRequestException {

    public InvalidDataException(String message) {
        super(message);
    }
}
