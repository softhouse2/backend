package pl.polsl.softhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.polsl.softhouse.entities.Task;
import pl.polsl.softhouse.entities.enums.WorkStatus;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query("SELECT t FROM Task t join t.worker w JOIN FETCH t.issue i " +
            "WHERE (:userId IS NULL OR w.id = :userId) AND (:status IS NULL OR t.status = :status) " +
            "AND t.dateOpened >= CAST(:startDt AS timestamp) AND t.dateOpened < CAST(:endDt AS timestamp) " +
            "AND (:issueId IS NULL OR i.id = :issueId)")
    List<Task> findAllTasksByUserIdAndStatusAndDateAndIssueId(
            Long userId,
            WorkStatus status,
            LocalDateTime startDt,
            LocalDateTime endDt,
            Long issueId);

    @Query("SELECT t FROM Task t join t.worker w JOIN FETCH t.issue i " +
            "WHERE (:userId IS NULL OR w.id = :userId) AND (:status IS NULL OR t.status = :status) " +
            "AND (:issueId IS NULL OR i.id = :issueId)")
    List<Task> findAllTasksByUserIdAndStatusAndIssueId(
            Long userId,
            WorkStatus status,
            Long issueId);
}
