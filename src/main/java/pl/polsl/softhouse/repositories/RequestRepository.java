package pl.polsl.softhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.polsl.softhouse.entities.Request;
import pl.polsl.softhouse.entities.enums.WorkStatus;

import java.time.LocalDateTime;
import java.util.List;

public interface RequestRepository extends JpaRepository<Request, Long> {
    @Query("SELECT r FROM Request r JOIN r.accountManager am " +
            "WHERE (:userId IS NULL OR am.id = :userId) " +
            "AND (:status IS NULL OR r.status = :status) " +
            "AND r.dateOpened >= CAST(:startDt AS timestamp) AND r.dateOpened < CAST(:endDt AS timestamp)")
    List<Request> findAllRequestsByUserIdAndStatusAndDate(
            Long userId,
            WorkStatus status,
            LocalDateTime startDt,
            LocalDateTime endDt);

    @Query("SELECT r FROM Request r JOIN r.accountManager am " +
            "WHERE (:userId IS NULL OR am.id = :userId) " +
            "AND (:status IS NULL OR r.status = :status) ")
    List<Request> findAllRequestsByUserIdAndStatus(
            Long userId,
            WorkStatus status);
}
