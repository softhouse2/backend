package pl.polsl.softhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.polsl.softhouse.entities.SystemVersion;

import java.util.List;

public interface SystemVersionRepository extends JpaRepository<SystemVersion, Long> {
    @Query(value = "SELECT sc FROM SystemVersion sc JOIN sc.system s JOIN sc.client c " +
            "WHERE (:systemId IS NULL OR s.id = :systemId) AND " +
            "(:clientId IS NULL OR c.id = :clientId)")
    List<SystemVersion> findBySystemIdOrClientId(Long systemId, Long clientId);
}
