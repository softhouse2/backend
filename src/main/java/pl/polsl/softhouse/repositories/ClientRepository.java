package pl.polsl.softhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.polsl.softhouse.entities.Client;

import java.util.List;

public interface ClientRepository extends JpaRepository<Client, Long> {
    @Query("SELECT c FROM SystemVersion sc JOIN sc.client c JOIN sc.system s " +
            "WHERE s.id = ?1")
    List<Client> findBySystemId(long systemId);
}
