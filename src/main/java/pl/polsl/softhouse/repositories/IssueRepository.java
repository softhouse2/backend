package pl.polsl.softhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.polsl.softhouse.entities.Issue;
import pl.polsl.softhouse.entities.enums.WorkStatus;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface IssueRepository extends JpaRepository<Issue, Long> {
    @Query("SELECT i FROM Issue i join i.productManager pm JOIN i.request r " +
            "WHERE (:userId IS NULL OR pm.id = :userId) AND (:status IS NULL OR i.status = :status) " +
            "AND i.dateOpened >= CAST(:startDt AS timestamp) AND i.dateOpened < CAST(:endDt AS timestamp) " +
            "AND (:requestId IS NULL OR r.id = :requestId)")
    List<Issue> findAllIssuesByUserIdAndStatusAndDateAndRequestId(
            Long userId,
            WorkStatus status,
            LocalDateTime startDt,
            LocalDateTime endDt,
            Long requestId);

    @Query("SELECT i FROM Issue i join i.productManager pm JOIN i.request r " +
            "WHERE (:userId IS NULL OR pm.id = :userId) AND (:status IS NULL OR i.status = :status) " +
            "AND (:requestId IS NULL OR r.id = :requestId)")
    List<Issue> findAllIssuesByUserIdAndStatusAndRequestId(
            Long userId,
            WorkStatus status,
            Long requestId);
}
