package pl.polsl.softhouse.entities;

import pl.polsl.softhouse.entities.enums.TaskType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tasks")
public class Task extends AbstractWorkUnit {
    private static final String GEN_NAME = "task_sequence";

    @Id
    @SequenceGenerator(name=GEN_NAME, allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator=GEN_NAME)
    private Long id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "issue_id", nullable = false)
    @NotNull
    private Issue issue;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity worker;

    @Enumerated(EnumType.ORDINAL)
    @Column(nullable = false)
    @NotNull
    private TaskType type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }

    public UserEntity getWorker() {
        return worker;
    }

    public void setWorker(UserEntity worker) {
        this.worker = worker;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }
}
