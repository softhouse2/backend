package pl.polsl.softhouse.entities.enums;

public enum TaskType {
    ANALYSIS,
    PROGRAMMING,
    TESTING,
    DOCS,
    DESIGN
}
