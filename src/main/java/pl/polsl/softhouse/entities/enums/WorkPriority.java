package pl.polsl.softhouse.entities.enums;

public enum WorkPriority {
    HIGH,
    NORMAL,
    LOW
}
