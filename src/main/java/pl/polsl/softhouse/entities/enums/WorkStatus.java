package pl.polsl.softhouse.entities.enums;

public enum WorkStatus {
    OPEN,
    IN_PROGRESS,
    FINISHED,
    CANCELED
}
