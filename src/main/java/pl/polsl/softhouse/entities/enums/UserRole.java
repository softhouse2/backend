package pl.polsl.softhouse.entities.enums;

public enum UserRole {
    ADMIN,
    ACCOUNT_MANAGER,
    PRODUCT_MANAGER,
    WORKER
}
