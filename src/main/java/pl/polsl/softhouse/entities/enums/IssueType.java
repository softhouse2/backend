package pl.polsl.softhouse.entities.enums;

public enum IssueType {
    FEATURE,
    BUG,
    IMPROVEMENT
}
