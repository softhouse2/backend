package pl.polsl.softhouse.entities.enums;

public enum SortingOption {
    NAME_ASC,
    NAME_DESC,
    DATE_ASC,
    DATE_DESC
}
